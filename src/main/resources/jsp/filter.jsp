<%@ page import="org.alche.servlet.PartsServlet" %>
<form action="search">
    <div style="width: 30em">
        <table frame="hsides">
            <caption class="grey">Filter</caption>
            <tr>
                <td>
                    <label for="${PartsServlet.PN}">PN</label>
                </td>
                <td><input id="${PartsServlet.PN}" name="${PartsServlet.PN}" type="text"
                           value="${sessionScope.get(PartsServlet.PN)}"></td>
            </tr>
            <tr>
                <td>
                    <label for="${PartsServlet.PART_NAME}">Part name</label>
                </td>
                <td><input id="${PartsServlet.PART_NAME}"
                           name="${PartsServlet.PART_NAME}" type="text"
                           value="${sessionScope.get(PartsServlet.PART_NAME)}"></td>
            </tr>
            <tr>
                <td>
                    <label for="${PartsServlet.VENDOR}">Vendor</label>
                </td>
                <td><input id="${PartsServlet.VENDOR}" name="${PartsServlet.VENDOR}" type="text"
                           value="${sessionScope.get(PartsServlet.VENDOR)}"></td>
            </tr>
            <tr>
                <td>
                    <label for="${PartsServlet.QTY}">Qty</label>
                </td>
                <td><input id="${PartsServlet.QTY}" name="${PartsServlet.QTY}" type="number"
                           value="${sessionScope.get(PartsServlet.QTY)}"></td>
            </tr>
            <tr>
                <td>
                    <label>Shipped</label>
                </td>
                <td>
                    <label for="${PartsServlet.SHIPPED_AFTER}">after</label> <input id="${PartsServlet.SHIPPED_AFTER}"
                                                                                    name="${PartsServlet.SHIPPED_AFTER}"
                                                                                    type="date"
                                                                                    value="${sessionScope.get(PartsServlet.SHIPPED_AFTER)}">
                    <label for="${PartsServlet.SHIPPED_BEFORE}">before</label> <input
                        id="${PartsServlet.SHIPPED_BEFORE}" name="${PartsServlet.SHIPPED_BEFORE}" type="date"
                        value="${sessionScope.get(PartsServlet.SHIPPED_BEFORE)}">
                </td>
            </tr>
            <tr>
                <td>
                    <label>Received</label>
                </td>
                <td>
                    <label for="${PartsServlet.RECEIVED_AFTER}">after</label> <input id="${PartsServlet.RECEIVED_AFTER}"
                                                                                     name="${PartsServlet.RECEIVED_AFTER}"
                                                                                     type="date"
                                                                                     value="${sessionScope.get(PartsServlet.RECEIVED_AFTER)}">
                    <label for="${PartsServlet.RECEIVED_BEFORE}">before</label> <input
                        id="${PartsServlet.RECEIVED_BEFORE}" name="${PartsServlet.RECEIVED_BEFORE}"
                        type="date"
                        value="${sessionScope.get(PartsServlet.RECEIVED_BEFORE)}">
                </td>
            </tr>
        </table>
        <button class="grey" type="submit" style="margin: auto; display: block">Filter</button>
    </div>
</form>