<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:useBean id="parts" scope="request" type="java.util.List<org.alche.model.Part>"/>

<table class="result-table sortable">
    <thead>
        <tr>
            <td>PN</td>
            <td>Part name</td>
            <td>Vendor</td>
            <td>Qty</td>
            <td>Shipped</td>
            <td>Received</td>
        </tr>
    </thead>
    <tbody>
    <c:forEach var="part" items="${parts}">
        <tr>
            <td>${part.name}</td>
            <td>${part.number}</td>
            <td>${part.vendor}</td>
            <td>${part.qty}</td>
            <td>${part.shipped}</td>
            <td>${part.received}</td>
        </tr>
    </c:forEach>
    </tbody>
</table>

<script type='text/javascript' src='js/common.js'></script>
<script type='text/javascript' src='js/css.js'></script>
<script type='text/javascript' src='js/standardista-table-sorting.js'></script>
