package org.alche.db;

import com.zaxxer.hikari.HikariDataSource;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;


public class DBConnectionFactory {
    private HikariDataSource hikariDataSource;

    public DBConnectionFactory(String driverClassName, String jdbcUrl, String login, String password) throws SQLException {
        hikariDataSource = new HikariDataSource();
        hikariDataSource.setDriverClassName(driverClassName);
        hikariDataSource.setJdbcUrl(jdbcUrl);
        if (login != null && password != null) {
            hikariDataSource.setUsername(login);
            hikariDataSource.setPassword(password);
        }
        hikariDataSource.setMaximumPoolSize(100);
        hikariDataSource.setAutoCommit(true);

        initializeDB();
    }

    public Connection getConnection() throws SQLException {
        return this.hikariDataSource.getConnection();
    }

    private void initializeDB() throws SQLException {
        String createQuery = "CREATE TABLE IF NOT EXISTS PARTS " +
                "(id      INTEGER PRIMARY KEY NOT NULL, " +
                " name    VARCHAR(255), " +
                " number  VARCHAR(255), " +
                " vendor  VARCHAR(255), " +
                " qty     INTEGER, " +
                " shipped DATE, " +
                " receive DATE)";
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        statement.execute(createQuery);
        connection.close();
    }
}
