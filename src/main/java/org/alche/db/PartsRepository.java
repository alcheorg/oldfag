package org.alche.db;

import org.alche.model.Part;

import java.sql.SQLException;
import java.util.List;

public interface PartsRepository {

    List<Part> getPartsByFilter(FilterParameters filterParameters);

    List<Part> getPartsAll();

    void generateInitialData() throws SQLException;
}
