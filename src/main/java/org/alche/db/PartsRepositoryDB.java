package org.alche.db;

import net.andreinc.mockneat.MockNeat;
import net.andreinc.mockneat.types.enums.StringType;
import org.alche.conf.Configuration;
import org.alche.model.Part;

import java.sql.*;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class PartsRepositoryDB implements PartsRepository {

    private final Configuration configuration;
    private final ZoneId zoneId = ZoneId.systemDefault();

    public PartsRepositoryDB(Configuration configuration) {
        this.configuration = configuration;
    }

    private Connection getConnection() throws SQLException {
        return this.configuration.getDbConnectionFactory().getConnection();
    }


    public List<Part> getPartsAll() {
        return getPartsByQuery("SELECT * FROM PARTS");
    }

    public List<Part> getPartsByFilter(FilterParameters filterParameters) {
        if (filterParameters.anyPresent()) {
            StringBuilder stringBuilder = new StringBuilder("SELECT * FROM PARTS p WHERE ");
            filterParameters.getName().ifPresent(name -> stringBuilder.append("p.name LIKE ").append(name).append(" AND "));
            filterParameters.getNumber().ifPresent(number -> stringBuilder.append("p.number = ").append(number).append(" AND "));
            filterParameters.getVendor().ifPresent(vendor -> stringBuilder.append("p.vendor = ").append(vendor).append(" AND "));
            filterParameters.getQty().ifPresent(qty -> stringBuilder.append("p.qty = ").append(qty).append(" AND "));
            filterParameters.getShipped_after().ifPresent(shippedAfter -> stringBuilder.append("p.shipped >= '").append(shippedAfter.atStartOfDay(zoneId).toEpochSecond()*1000).append("' AND "));
            filterParameters.getShipped_before().ifPresent(before -> stringBuilder.append("p.shipped <= '").append(before.atStartOfDay(zoneId).toEpochSecond()*1000).append("' AND "));
            filterParameters.getReceive_after().ifPresent(shippedAfter -> stringBuilder.append("p.receive >= '").append(shippedAfter.atStartOfDay(zoneId).toEpochSecond()*1000).append("' AND "));
            filterParameters.getReceive_before().ifPresent(before -> stringBuilder.append("p.receive <= '").append(before.atStartOfDay(zoneId).toEpochSecond()*1000).append("' AND "));
            stringBuilder.delete(stringBuilder.length() - 5, stringBuilder.length());
            System.out.println(stringBuilder.toString());
            return this.getPartsByQuery(stringBuilder.toString());
        }
        return getPartsAll();
    }

    public void generateInitialData() throws SQLException {
        ResultSet resultSet = getConnection().createStatement().executeQuery("SELECT COUNT(*) AS CO FROM PARTS");
        if (resultSet.next() && resultSet.getInt("CO") == 0) {
            List<Part> parts = generateParts();
            String INSERT_QUERY = "INSERT INTO PARTS (id, name, number, vendor, qty, shipped, receive)" +
                    "VALUES (?, ?, ?, ?, ?, ?, ?)";
            Connection connection = this.getConnection();
            for (int i = 0; i < parts.size(); i++) {
                Part part = parts.get(i);
                PreparedStatement preparedStatement = connection.prepareStatement(INSERT_QUERY);
                preparedStatement.setInt(1, i);
                preparedStatement.setString(2, part.getName());
                preparedStatement.setString(3, part.getNumber());
                preparedStatement.setString(4, part.getVendor());
                preparedStatement.setInt(5, part.getQty());
                preparedStatement.setDate(6, Date.valueOf(part.getShipped()));
                preparedStatement.setDate(7, Date.valueOf(part.getReceived()));
                preparedStatement.execute();
            }
            connection.close();
        }
    }

    private void insertPart(Part part, int id) {
        PreparedStatement preparedStatement;
        try {
            String INSERT_QUERY = "INSERT INTO PARTS (id, name, number, vendor, qty, shipped, receive)" +
                    "VALUES (?, ?, ?, ?, ?, ?, ?)";
            Connection connection = this.getConnection();
            preparedStatement = connection.prepareStatement(INSERT_QUERY);
            preparedStatement.setInt(1, id);
            preparedStatement.setString(2, part.getName());
            preparedStatement.setString(3, part.getNumber());
            preparedStatement.setString(4, part.getVendor());
            preparedStatement.setInt(5, part.getQty());
            preparedStatement.setDate(6, Date.valueOf(part.getShipped()));
            preparedStatement.setDate(7, Date.valueOf(part.getReceived()));
            preparedStatement.execute();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private List<Part> getPartsByQuery(String query) {
        try {
            Connection connection = this.getConnection();
            Statement selectStatement = connection.createStatement();
            ResultSet resultSet = selectStatement.executeQuery(query);
            LinkedList<Part> parts = new LinkedList<>();
            while (resultSet.next()) {
                Part part = new Part();
                part.setName(resultSet.getString("name"));
                part.setNumber(resultSet.getString("number"));
                part.setVendor(resultSet.getString("vendor"));
                part.setQty(resultSet.getInt("qty"));
                part.setShipped(resultSet.getDate("shipped").toLocalDate());
                part.setReceived(resultSet.getDate("receive").toLocalDate());
                parts.add(part);
            }
            connection.close();
            return parts;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    private static List<Part> generateParts() {
        MockNeat moc = MockNeat.threadLocal();
        return moc.filler(Part::new)
                .setter(Part::setName, moc.users())
                .setter(Part::setNumber, moc.strings().size(10).type(StringType.NUMBERS))
                .setter(Part::setQty, moc.ints().range(1, 20000))
                .setter(Part::setVendor, moc.departments())
                .setter(Part::setShipped, moc.localDates().past(LocalDate.of(1980, 1, 1)))
                .setter(Part::setReceived, moc.localDates().future(LocalDate.of(2090, 1, 1)))
                .list(10).val();
    }
}
