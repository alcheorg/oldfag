package org.alche.db;

import java.time.LocalDate;
import java.util.Optional;

public class FilterParameters {
    private Optional<String>    name;
    private Optional<String>    number;
    private Optional<String>    vendor;
    private Optional<Integer>   qty;
    private Optional<LocalDate> shipped_after;
    private Optional<LocalDate> shipped_before;
    private Optional<LocalDate> receive_after;
    private Optional<LocalDate> receive_before;

    public FilterParameters(Optional<String> name, Optional<String> number, Optional<String> vendor, Optional<Integer> qty, Optional<LocalDate> shipped_after, Optional<LocalDate> shipped_before, Optional<LocalDate> receive_after, Optional<LocalDate> receive_before) {
        this.name = name;
        this.number = number;
        this.vendor = vendor;
        this.qty = qty;
        this.shipped_after = shipped_after;
        this.shipped_before = shipped_before;
        this.receive_after = receive_after;
        this.receive_before = receive_before;
    }

    public Optional<String> getName() {
        return name;
    }

    public Optional<String> getNumber() {
        return number;
    }

    public Optional<String> getVendor() {
        return vendor;
    }

    public Optional<Integer> getQty() {
        return qty;
    }

    public Optional<LocalDate> getShipped_after() {
        return shipped_after;
    }

    public Optional<LocalDate> getShipped_before() {
        return shipped_before;
    }

    public Optional<LocalDate> getReceive_after() {
        return receive_after;
    }

    public Optional<LocalDate> getReceive_before() {
        return receive_before;
    }
    
    public boolean anyPresent() {
        return name.isPresent()
                || number.isPresent()
                ||vendor.isPresent()
                || qty.isPresent()
                || shipped_after.isPresent()
                || shipped_before.isPresent()
                || receive_after.isPresent()
                || receive_before.isPresent(); 
    }
}
