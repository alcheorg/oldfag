package org.alche.servlet;

import org.alche.conf.Configuration;
import org.alche.db.FilterParameters;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Optional;

public class PartsServlet extends HttpServlet {
    public final static String PN = "PN";
    public final static String PART_NAME = "Part_name";
    public final static String VENDOR = "Vendor";
    public final static String QTY = "Qty";
    public final static String SHIPPED_BEFORE = "Shipped_before";
    public final static String SHIPPED_AFTER = "Shipped_after";
    public final static String RECEIVED_BEFORE = "Received_before";
    public final static String RECEIVED_AFTER = "Received_after";
    public static final String PARTS = "parts";

    private Configuration configuration;

    public PartsServlet(Configuration configuration) {
        this.configuration = configuration;
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        Optional<String> partName = toOptional(req.getParameter(PART_NAME));
        Optional<String> partNumber = toOptional(req.getParameter(PN));
        Optional<String> vendor = toOptional(req.getParameter(VENDOR));
        Optional<String> qty = toOptional(req.getParameter(QTY));
        Optional<String> shippedAfter = toOptional(req.getParameter(SHIPPED_AFTER));
        Optional<String> shippedBefore = toOptional(req.getParameter(SHIPPED_BEFORE));
        Optional<String> receivedAfter = toOptional(req.getParameter(RECEIVED_AFTER));
        Optional<String> receivedBefore = toOptional(req.getParameter(RECEIVED_BEFORE));

        FilterParameters filterParameters = new FilterParameters(
                partName,
                partNumber,
                vendor,
                qty.map(Integer::parseInt),
                shippedAfter.map(LocalDate::parse),
                shippedBefore.map(LocalDate::parse),
                receivedAfter.map(LocalDate::parse),
                receivedBefore.map(LocalDate::parse)
        );

        req.getSession().setAttribute(PN, partNumber.orElse(""));
        req.getSession().setAttribute(PART_NAME, partName.orElse(""));
        req.getSession().setAttribute(VENDOR, vendor.orElse(""));
        req.getSession().setAttribute(QTY, qty.orElse(""));
        req.getSession().setAttribute(SHIPPED_AFTER, shippedAfter.orElse(""));
        req.getSession().setAttribute(SHIPPED_BEFORE, shippedBefore.orElse(""));
        req.getSession().setAttribute(RECEIVED_AFTER, receivedAfter.orElse(""));
        req.getSession().setAttribute(RECEIVED_BEFORE, receivedBefore.orElse(""));


        req.setAttribute(PARTS, this.configuration.getPartsRepository().getPartsByFilter(filterParameters));
        this.getServletContext().getRequestDispatcher("/searchPage.jsp").forward(req, resp);
    }

    private Optional<String> toOptional(String a) {
        return (a == null || "".equals(a)) ? Optional.empty() : Optional.of(a);
    }
}
