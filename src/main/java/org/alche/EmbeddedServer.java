package org.alche;

import org.alche.conf.Configuration;
import org.alche.db.DBConnectionFactory;
import org.alche.db.PartsRepositoryDB;
import org.alche.servlet.PartsServlet;
import org.apache.catalina.Context;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.startup.Tomcat;

import javax.servlet.ServletException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Objects;
import java.util.Properties;

public class EmbeddedServer {

    private static final String DEFAULT_SERVER_PORT = "8090";
    private static final String SERVER_PROPERTIES_FILE_NAME = "server.properties";
    private static final String DB_CLASS_NAME = "db.className";
    private static final String DB_JDBC_CONNECTION_URL = "db.jdbcConnectionUrl";
    private static final String DB_USER = "db.user";
    private static final String DB_PASSWORD = "db.password";
    private static final String SERVER_PORT = "port";

    public static void main(String[] args) throws LifecycleException, IOException, ServletException, SQLException {
        Properties props = new Properties();
        props.load(EmbeddedServer.class.getClassLoader().getResourceAsStream(SERVER_PROPERTIES_FILE_NAME));

        Configuration configuration = new Configuration();
        configuration.setDbConnectionFactory(new DBConnectionFactory(
                props.getProperty(DB_CLASS_NAME),
                props.getProperty(DB_JDBC_CONNECTION_URL),
                props.getProperty(DB_USER),
                props.getProperty(DB_PASSWORD)));
        configuration.setPartsRepository(new PartsRepositoryDB(configuration));
        configuration.getPartsRepository().generateInitialData();

        Tomcat tomcat = new Tomcat();
        tomcat.setPort(Integer.valueOf(props.getProperty(SERVER_PORT, DEFAULT_SERVER_PORT)));

        Context ctx = tomcat.addWebapp("/", Objects.requireNonNull(EmbeddedServer.class.getClassLoader().getResource("jsp")).getFile());

        Tomcat.addServlet(ctx, "embedded", new PartsServlet(configuration));
        ctx.addServletMapping("/search", "embedded");
        tomcat.start();
        tomcat.getServer().await();
    }
}
