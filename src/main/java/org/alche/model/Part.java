package org.alche.model;

import java.time.LocalDate;
import java.util.Objects;

public class Part {
    private String     name;
    private String     number;
    private String     vendor;
    private Integer    qty;
    private LocalDate  shipped;
    private LocalDate  received;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public LocalDate getShipped() {
        return shipped;
    }

    public void setShipped(LocalDate shipped) {
        this.shipped = shipped;
    }

    public LocalDate getReceived() {
        return received;
    }

    public void setReceived(LocalDate received) {
        this.received = received;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Part part = (Part) o;
        return Objects.equals(name, part.name) &&
                Objects.equals(number, part.number) &&
                Objects.equals(vendor, part.vendor) &&
                Objects.equals(qty, part.qty) &&
                Objects.equals(shipped, part.shipped) &&
                Objects.equals(received, part.received);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, number, vendor, qty, shipped, received);
    }

    @Override
    public String toString() {
        return "Part{" +
                "name='" + name + '\'' +
                ", number='" + number + '\'' +
                ", vendor='" + vendor + '\'' +
                ", qty=" + qty +
                ", Shipped=" + shipped +
                ", Receive=" + received +
                '}';
    }
}
