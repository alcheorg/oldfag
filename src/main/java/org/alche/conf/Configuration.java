package org.alche.conf;

import org.alche.db.DBConnectionFactory;
import org.alche.db.PartsRepository;

public class Configuration {
    private DBConnectionFactory dbConnectionFactory;
    private PartsRepository partsRepository;

    public DBConnectionFactory getDbConnectionFactory() {
        return dbConnectionFactory;
    }

    public void setDbConnectionFactory(DBConnectionFactory dbConnectionFactory) {
        this.dbConnectionFactory = dbConnectionFactory;
    }

    public PartsRepository getPartsRepository() {
        return partsRepository;
    }

    public void setPartsRepository(PartsRepository partsRepository) {
        this.partsRepository = partsRepository;
    }
}
